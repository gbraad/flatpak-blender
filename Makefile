GPG_KEYID := E8CFA3B3C849DA4567EC56182A41910290C72C72
REPO := ../repo-apps
BUILD_DIR := ./appdir
BUILD_CMD = flatpak-builder --ccache --force-clean --gpg-sign=$(GPG_KEYID) --repo=$(REPO) $(BUILD_DIR) $<


blender: org.blender.app.json
	$(BUILD_CMD)


blender-nightly: org.blender.app.nightly.json
	$(BUILD_CMD)
